var assert = require('assert');
const pow = require('./pow');

describe('POW', function () {
  describe('base test -1,0,1', function () {
    it('should return 0.5 when pow base 2 and the value is -1', function () {
      assert.equal(pow(2, -1), 0.5);
    });
    it('should return 1 when pow base 2 and the value is 0', function () {
      assert.equal(pow(2, 0), 1);
    });
    it('should return 2 when pow base 2 and the value is 1', function () {
      assert.equal(pow(2, 1), 2);
    });
  });

  describe('base test [up] on positive range', function () {
    it('should return 2 when pow base 2 and the value is 1', function () {
      assert.equal(pow(2, 1), 2);
    });
    it('should return 8 when pow base 2 and the value is 3', function () {
      assert.equal(pow(2, 3), 8);
    });
    it('should return 1024 when pow base 2 and the value is 10', function () {
      assert.equal(pow(2, 10), 1024);
    });
    it('should return 1.2676506002282294e+30 when pow base 2 and the value is 100', function () {
      assert.equal(pow(2, 100), 1.2676506002282294e+30);
    });
    it('should return 1.0715086071862673e+301 when pow base 2 and the value is 1000', function () {
      assert.equal(pow(2, 1000), 1.0715086071862673e+301);
    });
    it('should return 1.0972248137587377e+304 when pow base 2 and the value is 1010', function () {
      assert.equal(pow(2, 1010), 1.0972248137587377e+304);
    });
  });
  describe('base test [up] on negative range', function () {
    it('should return 2 when pow base 2 and the value is -1', function () {
      assert.equal(pow(2, -1), 1 / 2);
    });
    it('should return 8 when pow base 2 and the value is -3', function () {
      assert.equal(pow(2, -3), 1 / 8);
    });
    it('should return 1024 when pow base 2 and the value is -10', function () {
      assert.equal(pow(2, -10), 1 / 1024);
    });
    it('should return 1.2676506002282294e+30 when pow base 2 and the value is -100', function () {
      assert.equal(pow(2, -100), 1 / 1.2676506002282294e+30);
    });
    it('should return 1.0715086071862673e+301 when pow base 2 and the value is -1000', function () {
      assert.equal(pow(2, -1000), 1 / 1.0715086071862673e+301);
    });
    it('should return 1.0972248137587377e+304 when pow base 2 and the value is -1010', function () {
      assert.equal(pow(2, -1010), 1 / 1.0972248137587377e+304);
    });
  });
});