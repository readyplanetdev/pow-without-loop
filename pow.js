module.exports = pow

function pow(base, up) {
  var result = 0;
  var start = new Date().getTime();
  if (up === 0) return 1;
  if (up === 1) return base;
  result = ((up > 0) ? base * pow(base, up - 1) : 1 / (base / pow(base, up + 1)));
  return result;
}