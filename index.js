const pow = require('./pow');

var newline = '\n================================';
console.log('result pow(2, -1)', pow(2, -1), newline);
console.log('result pow(2, 0)', pow(2, 0), newline);
console.log('result pow(2, 1)', pow(2, 1), newline);
console.log('result pow(2, 3)', pow(2, 3), newline);
console.log('result pow(2, 30)', pow(2, 30), newline);
console.log('result pow(2, 1000)', pow(2, 1000), newline);
console.log('result pow(2, 1100)', pow(2, 1100), newline);
console.log('result pow(2, -3)', pow(2, -3), newline);
console.log('result pow(2, -30)', pow(2, -30), newline);
console.log('result pow(2, -1000)', pow(2, -1000), newline);
console.log('result pow(2, -1100)', pow(2, -1100), newline);